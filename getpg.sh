#!/bin/bash
set -x

# 20220420 DAF Version inicial
#              Usar: bash $0 INVENTORY_FILE 13

INVENTORY_FILE=${1:-"./hosts.ini"}
mypgversion=${2:-14}
mypgprimary=${3:-daflab-1}

vagrant up 

echo "[primary]" > $INVENTORY_FILE
grep "10.0.0" /etc/hosts | sort | head -n 1 | awk '{print $2 " ansible_host="$1}' >> $INVENTORY_FILE
echo " " >> $INVENTORY_FILE
echo "[replica]" >> $INVENTORY_FILE
grep "10.0.0" /etc/hosts | sort | grep -v $(grep "10.0.0" /etc/hosts | sort | head -n 1 | awk '{print $1}')  | awk '{print $2 " ansible_host="$1}' >> $INVENTORY_FILE

#ansible all -i ./hosts.yaml -u vagrant -m shell -a 'echo "$(hostname) - $(hostname -I)"'
ansible all -u vagrant -m shell -a 'echo "$(hostname) - $(hostname -I)"' -i $INVENTORY_FILE
ansible all -u vagrant -m ping -i $INVENTORY_FILE

ansible-playbook -u vagrant postgresql-primary.yml  --extra-vars "mypgversion=$mypgversion" -i $INVENTORY_FILE
ansible-playbook -u vagrant postgresql-replica.yml  --extra-vars "mypgversion=$mypgversion mypgprimary=$mypgprimary" -i $INVENTORY_FILE


INVENTORY_FILE=./hosts.ini
mypgversion=14
mypgprimary=daflab-1

pgbench --initialize --scale=10000 --init-steps=dtgpf --username=daf -d daf --host=$mypgprimary
pgbench  --builtin=tpcb-like   --client=6   --jobs=6   --time=300   --progress=5 --host=$mypgprimary   --username=daf daf
